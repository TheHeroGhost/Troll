package Listeners;

import de.TheHeroGhost.main.main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class event_freeze implements Listener {

    @EventHandler
    public void on(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if(main.getFreeze().contains(p)) {
            e.setCancelled(true);
        }
    }
}
