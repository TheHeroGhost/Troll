package Listeners;

import de.TheHeroGhost.main.main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class event_fakeop implements Listener {

    @EventHandler
    public void on(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        if(main.getFakeOp().contains(p)) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                if(main.getFakeOpShow().contains(all)) {
                    all.sendMessage(main.getPr() + "§a" + p.getName() + "§7: §4" + e.getMessage());
                }
            }
        }
    }
}
