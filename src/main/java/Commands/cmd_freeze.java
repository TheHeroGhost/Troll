package Commands;

import de.TheHeroGhost.main.main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class cmd_freeze implements CommandExecutor {

    private String pr = main.getPr();
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        if(args.length == 0) {
            p.sendMessage(pr + "§a/freeze [Spieler]");
        } else {
            if (args.length == 1) {
                Player p2 = Bukkit.getPlayer(args[0]);
                if (p2 != null) {
                    if (main.getFreeze().contains(p2)) {
                        main.getFreeze().remove(p2);
                        p.sendMessage(pr + "Der Spieler §a" + p2.getName() + "§b ist nun nichtmehr Gefreezed");
                    } else {
                        main.getFreeze().add(p);
                        p.sendMessage(pr + "Der Spieler §a" + p2.getName() + "§b ist nun Gefreezed!");
                    }
                } else {
                    p.sendMessage(pr + "§cDer Spieler muss Online sein!");
                }
            } else {
                if(args.length > 1) {
                    p.sendMessage(pr + "§a/freeze [Spieler]");
                }
            }
        }
        return false;
    }
}

