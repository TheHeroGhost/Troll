package de.TheHeroGhost.main;

import Commands.*;
import Listeners.event_fakeop;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import Listeners.event_freeze;

import java.util.ArrayList;

@Getter
public class main extends JavaPlugin implements Listener, CommandExecutor {

    @Getter
    public static ArrayList<Player> Freeze = new ArrayList<>();

    @Getter
    public static ArrayList<Player> FakeOp = new ArrayList<>();

    @Getter
    public static ArrayList<Player> FakeOpShow = new ArrayList<>();

    @Getter
    public static  String pr = "§7[§4Troll-Plugin§7]§b ";

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        registerCommands();
        registerListener();
    }

    @Override
    public void onDisable() {

    }

    public void registerCommands() {
        getCommand("troll").setExecutor(new cmd_troll()); //Auskunft!
        getCommand("crash").setExecutor(new cmd_crash()); // /crash [Spieler]
        getCommand("dc").setExecutor(new cmd_dc()); // /dc [Spieler]
        getCommand("fakeop").setExecutor(new cmd_fakeop());// /fakeop [Spieler] || /FakeOP show
        getCommand("freeze").setExecutor(new cmd_freeze());// /Freeze [Spieler]
    }

    public void registerListener() {
        Bukkit.getPluginManager().registerEvents(new event_freeze(), this); // Freeze
        Bukkit.getPluginManager().registerEvents(new event_fakeop(), this); //FakeOP show
    }

    public void help(Player p) {
        p.sendMessage(pr + "§a/freeze [Spieler] §bFriert einen Spieler ein!");
        p.sendMessage(pr + "§a/dc [Spieler] §b Lässt einen Spieler disconnecten!");
        p.sendMessage(pr + "§a/crash [Spieler] §bLässt einen Spieler crashen!");
        p.sendMessage(pr + "§a/fakeop [Spieler] §bGibt einem Spieler FakeOP!");
        p.sendMessage(pr + "§a/fakeop show §bZeigt dir die Befehle der FakeOP Spieler an!" );
    }

}
